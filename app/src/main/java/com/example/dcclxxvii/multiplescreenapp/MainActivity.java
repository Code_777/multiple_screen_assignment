package com.example.dcclxxvii.multiplescreenapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button screen1,screen2, screen3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //views
        screen1 = (Button) findViewById(R.id.btn_screen1);
        screen2 = (Button) findViewById(R.id.btn_screen2);
        screen3 = (Button) findViewById(R.id.btn_screen3);

        //register listeners
        screen1.setOnClickListener(this);
        screen2.setOnClickListener(this);
        screen3.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == screen1){

            //open screen 1, reason image
            startActivity(new Intent(MainActivity.this, Screen1_Reason.class));


        }else if(v == screen2){

            //open screen 2, cognition image
            startActivity(new Intent(MainActivity.this, Screen2_Cognition.class));

        }else if(v == screen3){

            //open screen 3, caring image
            startActivity(new Intent(MainActivity.this, Screen3_Caring.class));
        }

    }
}
