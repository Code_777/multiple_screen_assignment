package com.example.dcclxxvii.multiplescreenapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Screen3_Caring extends AppCompatActivity {

    Button close ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen3__caring);

        //views
        close = (Button) findViewById(R.id.btn_screen3_close);

        //attach listener, to close the activity
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
